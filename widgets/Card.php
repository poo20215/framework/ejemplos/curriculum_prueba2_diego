<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;
use yii\base\Widget;

/**
 * Description of card
 *
 * @author Alpe
 */
class Card extends Widget{
    
    public $titulo;
    public $contenido;
    
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $salida='<div class="card col-lg-12 p-0 mt-2">';
        $salida.='<div class="card-header font-weight-bold">' . $this->titulo . '</div>';
        $salida.='<div class="card-body">' . $this->contenido . '</div>';
        $salida.='</div>';
        return $salida; 
    }
}
