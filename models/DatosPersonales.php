<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "datos_personales".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $telefono
 * @property string|null $email
 * @property string|null $direccion
 * @property int|null $carnet_de_conducir
 * @property string|null $foto
 */
class DatosPersonales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'datos_personales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carnet_de_conducir'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['apellidos', 'email'], 'string', 'max' => 100],
            [['telefono'], 'string', 'max' => 9],
            [['direccion'], 'string', 'max' => 250],
            [['foto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png,jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'telefono' => 'Telefono',
            'email' => 'Email',
            'direccion' => 'Direccion',
            'carnet_de_conducir' => 'Carnet De Conducir',
            'foto' => 'Cambiar foto',
        ];
    }
    
    public function getNombreCompleto()
    {
        return "$this->nombre $this->apellidos";
    }
}
