<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "funciones".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $id_experiencia
 *
 * @property Experiencia $experiencia
 */
class Funciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'funciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_experiencia'], 'integer'],
            [['descripcion'], 'string', 'max' => 250],
            [['id_experiencia'], 'exist', 'skipOnError' => true, 'targetClass' => Experiencia::className(), 'targetAttribute' => ['id_experiencia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'id_experiencia' => 'Id Experiencia',
        ];
    }

    /**
     * Gets query for [[Experiencia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExperiencia()
    {
        return $this->hasOne(Experiencia::className(), ['id' => 'id_experiencia']);
    }
}
