<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "experiencia".
 *
 * @property int $id
 * @property string|null $fechas
 * @property string|null $empresa
 * @property string|null $puesto
 * @property string|null $poblacion
 *
 * @property Funciones[] $funciones
 */
class Experiencia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'experiencia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechas'], 'string', 'max' => 75],
            [['empresa'], 'string', 'max' => 100],
            [['puesto'], 'string', 'max' => 250],
            [['poblacion'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fechas' => 'Fechas',
            'empresa' => 'Empresa',
            'puesto' => 'Puesto',
            'poblacion' => 'Poblacion',
        ];
    }

    /**
     * Gets query for [[Funciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFunciones()
    {
        return $this->hasMany(Funciones::className(), ['id_experiencia' => 'id']);
    }
}
