<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "competencias".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property int|null $id_estudios
 *
 * @property Estudios $estudios
 */
class Competencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'competencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_estudios'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['id_estudios'], 'exist', 'skipOnError' => true, 'targetClass' => Estudios::className(), 'targetAttribute' => ['id_estudios' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'id_estudios' => 'Id Estudios',
        ];
    }

    /**
     * Gets query for [[Estudios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstudios()
    {
        return $this->hasOne(Estudios::className(), ['id' => 'id_estudios']);
    }
}
