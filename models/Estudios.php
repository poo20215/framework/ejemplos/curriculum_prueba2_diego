<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudios".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $fechas
 * @property string|null $centro
 * @property int|null $complementarios
 *
 * @property Competencias[] $competencias
 */
class Estudios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complementarios'], 'integer'],
            [['titulo', 'fechas'], 'string', 'max' => 100],
            [['centro'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'fechas' => 'Fechas',
            'centro' => 'Centro',
            'complementarios' => 'Complementarios',
        ];
    }

    /**
     * Gets query for [[Competencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompetencias()
    {
        return $this->hasMany(Competencias::className(), ['id_estudios' => 'id']);
    }
}
