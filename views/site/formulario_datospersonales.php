<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DatosPersonales */
/* @var $form ActiveForm */
?>
<div class="formulario_datospersonales">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'carnet_de_conducir')->checkbox() ?>
        <?= $form->field($model, 'nombre') ?>
        <?= $form->field($model, 'apellidos') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'telefono') ?>
        <?= $form->field($model, 'direccion') ?>
        <?= $form->field($model, 'foto')->fileInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Editar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario_datospersonales -->
