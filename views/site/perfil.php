<?php
use app\widgets\Card;
use yii\helpers\Html;
?>
<div class="row">

    <div class="col-lg-6">
<?php

echo Card::widget([
    //"titulo" => "Imagen",
    "contenido" => Html::img("@web/imgs/$model->foto",["class"=>"img-fluid","width"=>100,"height"=>200]),
]);
        
echo Card::widget([
    "titulo" => "Nombre Completo",
    "contenido" => $model->getNombreCompleto(),
]);

?>
    </div>
    <div >
<?php        
echo Card::widget([
    "titulo" => "Direccion",
    "contenido" => $model->direccion,
]);

echo Card::widget([
    "titulo" => "Correo Electrónico",
    "contenido" => $model->email,
]);

echo Card::widget([
    "titulo" => "Teléfono",
    "contenido" => $model->telefono,
]);

if($model->carnet_de_conducir)
{
    echo Card::widget([
    "titulo" => "Carnet de conducir",
    "contenido" => "Si",
]);
}

echo Html::a("Editar datos personales",["site/editardatospersonales"], ['class' => 'btn btn-primary float-right']);
?>
    </div>
    
</div>

