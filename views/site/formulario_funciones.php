<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Funciones */
/* @var $form ActiveForm */
?>
<div class="site-formulario_funciones">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'descripcion') ?>
    
        <div class="form-group">
            <?= Html::submitButton($boton, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario_funciones -->
