<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Experiencia */
/* @var $form ActiveForm */
?>
<div class="site-formulario_experiencia">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fechas') ?>
        <?= $form->field($model, 'empresa') ?>
        <?= $form->field($model, 'puesto') ?>
        <?= $form->field($model, 'poblacion') ?>
    
        <div class="form-group">
            <?= Html::submitButton($boton, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario_experiencia -->
