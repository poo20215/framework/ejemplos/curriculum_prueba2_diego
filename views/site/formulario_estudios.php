<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Estudios */
/* @var $form ActiveForm */
?>
<div class="site-formulario_estudios">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'titulo') ?>
        <?= $form->field($model, 'fechas') ?>
        <?= $form->field($model, 'centro') ?>
    
        <div class="form-group">
            <?= Html::submitButton($boton, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario_estudios -->
