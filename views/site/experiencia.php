<?php
use yii\widgets\ListView;
use yii\helpers\Html;
?>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_experiencia',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'caja',
    ],
    'layout'=>"{items}"

    ]);

echo Html::a('Añadir experiencia',["site/anadirexperiencia"], ['class' => 'btn btn-primary float-right']);
