<?php
use yii\helpers\Html;

echo "$model->fechas $model->puesto" . " " . "$model->empresa ($model->poblacion) " . 
        Html::a('<i class="fas fa-pen"></i>',["site/actualizarexperiencia","id" => $model->id]) . " " .
        Html::a('<i class="fas fa-trash-alt"></i>',["site/eliminarexperiencia","id" => $model->id],
                ['data' => [
                'confirm' => '¿Estas seguro que deseas eliminar esta experiencia?',
                'method' => 'post',
            ],
                    ]) . "&nbsp&nbsp&nbsp&nbsp" .
        Html::a('<i class="fas fa-plus"></i>',["site/anadirfunciones","id_experiencia" => $model->id]);

$funciones=$model->getFunciones()->all();

echo "<ul>";
foreach ($funciones as $funcion)
{
    echo "<li>$funcion->descripcion " . 
            Html::a('<i class="fas fa-pen"></i>',["site/actualizarfunciones","id" => $funcion->id]) . " " .
            Html::a('<i class="fas fa-trash-alt"></i>',["site/eliminarfunciones","id" => $funcion->id],
                ['data' => [
                'confirm' => '¿Estas seguro que deseas eliminar esta funcion?',
                'method' => 'post',
            ],
                    ]) .
            "</li>";
}
echo "</ul>";