<?php
use yii\helpers\Html;

echo $model->nombre . " " . $model->nivel . " " .
        Html::a('<i class="fas fa-pen"></i>',["site/actualizarotros","id" => $model->id]) . " " .
        Html::a('<i class="fas fa-trash-alt"></i>',["site/eliminarotros","id" => $model->id],
            ['data' => [
            'confirm' => '¿Estas seguro que deseas eliminar este dato?',
            'method' => 'post',
        ],
                ]);