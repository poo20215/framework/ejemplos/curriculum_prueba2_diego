<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Competencias */
/* @var $form ActiveForm */
?>
<div class="site-formulario_competencias">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'descripcion') ?>
    
        <div class="form-group">
            <?= Html::submitButton($boton, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario_competencias -->
