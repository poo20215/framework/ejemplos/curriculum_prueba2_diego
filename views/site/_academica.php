<?php
use yii\helpers\Html;
?>

<div class="row">    
    <div class="col-lg-4"><?= $model->fechas ?></div>
    <div class="col-lg-15"><?= $model->titulo . " ($model->centro) " . 
        Html::a('<i class="fas fa-pen"></i>',["site/actualizarestudios","id" => $model->id]) . " " .
        Html::a('<i class="fas fa-trash-alt"></i>',["site/eliminarestudios","id" => $model->id],
                ['data' => [
                'confirm' => '¿Estas seguro que deseas eliminar este estudio?',
                'method' => 'post',
            ],
                    ]) . "&nbsp&nbsp&nbsp&nbsp" .
        Html::a('<i class="fas fa-plus"></i>',["site/anadircompetencias","id_estudios" => $model->id, "formacion" => $model->complementarios])
        ?></div>
</div>

<?php

$competencias=$model->competencias; //otra forma de llamar al metodo getCompetencias()
echo '<ul>';
foreach($competencias as $competencia)
{
    echo "<li>$competencia->descripcion . " . 
            Html::a('<i class="fas fa-pen"></i>',["site/actualizarcompetencias","id" => $competencia->id, "formacion" => $model->complementarios]) . " " .
            Html::a('<i class="fas fa-trash-alt"></i>',["site/eliminarcompetencias","id" => $competencia->id, "formacion" => $model->complementarios],
                ['data' => [
                'confirm' => '¿Estas seguro que deseas eliminar esta competencia?',
                'method' => 'post',
            ],
                    ]) .
            "</li>";
}
echo '</ul>';

?>