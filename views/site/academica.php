<?php
use yii\widgets\ListView;
use yii\helpers\Html;
?>

<div>
    <h2><?= $titulo ?></h2>
</div>

<?php

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_academica',
    "itemOptions" => [
        'class' => 'col-lg-5 ml-auto mr-auto bg-light p-3 mb-5',
    ],
    "options" => [
        'class' => 'caja',
    ],
    'layout'=>"{items}"

    ]);

if($titulo=="Formación Académica")
{
    $formacion=0;
}else
{
   $formacion=1; 
}
echo Html::a('Añadir estudios',["site/anadirestudios","formacion" =>$formacion], ['class' => 'btn btn-primary float-right']);
